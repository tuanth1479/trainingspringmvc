DROP SCHEMA if exists `test`;
create schema test;
use test;

CREATE table users (
    id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    email VARCHAR(30) UNIQUE,
    password VARCHAR(300) NOT NULL,
    avatar VARCHAR(300) DEFAULT '/avatar/avatar_default.jpg',
    actived BOOLEAN DEFAULT 0
);

create table categories(
	cat_id int unsigned auto_increment primary key,
    cat_name varchar(20) not null,
    cat_detail varchar(50)
);

CREATE table products (
    product_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    cat_id INT UNSIGNED NULL,
    product_name VARCHAR(20) NOT NULL,
    product_image varchar(100),
    list_price DECIMAL(10 , 2 ) NOT NULL,
    is_trend boolean,
    discount_percent INT UNSIGNED,
    FOREIGN KEY (cat_id)
        REFERENCES categories (cat_id)
        ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE table company_info (
    id INT AUTO_INCREMENT PRIMARY KEY,
    phone VARCHAR(20) NOT NULL,
    email VARCHAR(30) NOT NULL,
    open_time VARCHAR(20) NOT NULL,
    address VARCHAR(50) NOT NULL
);

CREATE table feedbacks (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    email VARCHAR(30) NOT NULL,
    message VARCHAR(100) NOT NULL
);

CREATE table carts (
    cart_id INT UNSIGNED AUTO_INCREMENT,
    user_id BIGINT UNSIGNED,
    cart_status BOOLEAN DEFAULT 1,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (cart_id , user_id)
);

CREATE table cart_details (
    cart_id INT UNSIGNED,
    product_id INT UNSIGNED,
    product_name varchar(20),
    quantity INT,
    list_price DECIMAL(10 , 2 ),
    final_price double,
    discount_percent DECIMAL(10 , 2 ),
    PRIMARY KEY (cart_id , product_id),
    FOREIGN KEY (cart_id)
        REFERENCES carts (cart_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (product_id)
        REFERENCES products (product_id)
        ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE table orders (
    order_id INT UNSIGNED AUTO_INCREMENT,
    cart_id INT UNSIGNED,
    user_id BIGINT UNSIGNED,
    order_price double,
    order_status BOOLEAN DEFAULT 1,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (order_id , cart_id),
    FOREIGN KEY (cart_id)
        REFERENCES carts (cart_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE ON UPDATE CASCADE
);

insert into users (email, name, avatar, password) values ("admin@mail", "admin", "/avatar/avatar_default.jpg", "$2a$10$tZTkKvHF6E2m9ETXfQ8UeOBASgxoShFMrZHuhMbrZxoXvXblSBT7G");
insert into company_info (phone, email, open_time, address) values ("+8419001001","tuanth.nc.uit@gmail.com","7:30 am to 18:00 pm","Cong Hoa, Tan Binh, HCM");


insert into categories (cat_id, cat_name) value (1, "Phone"); 
insert into categories (cat_id, cat_name) value (2, "Laptop"); 
insert into categories (cat_id, cat_name) value (3, "Tablet");
 
insert into products (cat_id, product_name, product_image ,list_price, is_trend, discount_percent ) values ( 1, "Iphone 1", "/img/product/default.png", 1887, 1, 45 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 1, "Iphone 2", "/img/product/default.png", 141, 1, 30 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 1, "Iphone 3", "/img/product/default.png", 111, 1, 10 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 1, "Iphone 4", "/img/product/default.png", 161, 0, 0 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values (1, "Iphone 5", "/img/product/default.png", 211, 1, 20 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 1, "Iphone 6", "/img/product/default.png", 501, 1, 15 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values (1, "Iphone 7", "/img/product/default.png", 251, 0, 32 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values (1, "Iphone 8", "/img/product/default.png", 253, 0, 33 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 1, "OPPO A7", "/img/product/default.png", 122, 1, 12 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values (1, "Iphone 10", "/img/product/default.png", 104, 1, 12 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 1, "Nokia 1", "/img/product/default.png", 259.3, 1, 30 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values (1, "Ipad 1", "/img/product/default.png", 139, 0, 0 );
insert into products (cat_id, product_name, product_image ,list_price, is_trend, discount_percent ) values ( 1, "Nokia 2", "/img/product/default.png", 128, 1, 20 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 1, "Nokia 3", "/img/product/default.png", 109, 1, 30 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 1, "Nokia 4", "/img/product/default.png", 101, 1, 10 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 1, "Iphone 11", "/img/product/default.png", 149, 0, 0 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values (1, "Iphone 11", "/img/product/default.png", 209, 1, 20 );
insert into products ( cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 1, "Iphone 11", "/img/product/default.png", 499, 1, 15 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values (1, "Iphone 11", "/img/product/default.png", 249, 0, 32 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Asus 1", "/img/product/default.png", 1111, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Dell 1", "/img/product/default.png", 1112, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Asus 2", "/img/product/default.png", 134, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Asus 4", "/img/product/default.png", 5000, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Asus 5", "/img/product/default.png", 1177, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Asus 3", "/img/product/default.png", 4000, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Asus7", "/img/product/default.png", 1181, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Asus 5", "/img/product/default.png", 1138, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Asus 8", "/img/product/default.png", 2118, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Asus 8" , "/img/product/default.png", 4118, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Dell 3", "/img/product/default.png", 1518, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Dell 5", "/img/product/default.png", 1618, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Acer 1", "/img/product/default.png", 5000, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Acer 2", "/img/product/default.png", 1188, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Acer 4", "/img/product/default.png", 1233, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Acer 8", "/img/product/default.png", 1188, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 2, "Laptop Acer 10", "/img/product/default.png", 4200, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet Apple", "/img/product/default.png", 1117, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet Apple 1", "/img/product/default.png", 1617, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet Apple 2", "/img/product/default.png", 6117, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet Apple 3", "/img/product/default.png", 7117, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet Apple 4", "/img/product/default.png", 1117, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet Apple 5", "/img/product/default.png", 1118, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet Apple 6", "/img/product/default.png", 14200, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet Apple 7", "/img/product/default.png", 3187, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet Apple 9", "/img/product/default.png", 4117, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet SamSung 1 ", "/img/product/default.png", 4200, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet SamSung 2", "/img/product/default.png", 2117, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet SamSung 4", "/img/product/default.png", 1317, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet SamSung 5", "/img/product/default.png", 1617, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet SamSung 6", "/img/product/default.png", 4200, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet SamSung 7", "/img/product/default.png", 117, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet SamSung 8", "/img/product/default.png", 4200, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet SamSung 9", "/img/product/default.png", 1878, 1, 22 );
insert into products (cat_id, product_name, product_image, list_price, is_trend, discount_percent ) values ( 3, "Tablet SamSung 10", "/img/product/default.png", 1174, 1, 22 );

insert into carts (user_id) values (1);

insert into cart_details (cart_id, product_id, product_name, quantity, list_price, final_price, discount_percent) values (1, 1, "Iphone 11", 1, 100, 90, 10);
insert into cart_details (cart_id, product_id, product_name, quantity, list_price, final_price, discount_percent) values (1, 2, "Iphone 10", 1, 100, 90, 10); 