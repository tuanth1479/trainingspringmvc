package com.training.sys.dto;

import com.training.sys.model.CartDetail;

public class CartDTO {
	private int cartId;
	private int productId;
	private int quantity;
	private CartDetail cartDetail;
	

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "CartDTO [cartId=" + cartId + ", productId=" + productId + ", quantity=" + quantity + ", cartDetail="
				+ cartDetail + "]";
	}

	public CartDetail getCartDetail() {
		return cartDetail;
	}

	public void setCartDetail(CartDetail cartDetail) {
		this.cartDetail = cartDetail;
	}

	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

}
