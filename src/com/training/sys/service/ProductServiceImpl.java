package com.training.sys.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.sys.dao.ProductDao;
import com.training.sys.model.Product;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	ProductDao productDao;

	public List<Product> selectProductList() {
		return productDao.selectProductList();
	}

	public List<Product> selectProductTopTrend() {
		List<Product> topTrendList = new ArrayList<>();
		topTrendList = productDao.selectProductTopTrend();

		calculateListProductFinalPrice(topTrendList);

		return topTrendList;
	}

	public List<Product> selectProductTopDiscount() {
		List<Product> topDiscount = new ArrayList<>();
		topDiscount = productDao.selectProductTopDiscount();

		calculateListProductFinalPrice(topDiscount);

		return topDiscount;
	}

	public List<Product> selectProductByQueryWithPagination(Map<String, Object> map) {
		List<Product> products = new ArrayList<>();
		products = productDao.selectProductByQueryWithPagination(map);
		calculateListProductFinalPrice(products);
		return products;
	}

	public Product selectProductById(int productId) {

		Product product = productDao.selectProductById(productId);

		// calculate final price for 1 product
		double discountPrice = (product.getListPrice().doubleValue() * product.getDiscountPercent()) / 100;
		double finalPrice = product.getListPrice().doubleValue() - discountPrice;
		product.setFinalPrice(finalPrice);

		return product;

	}

	public Integer getNumberOfProductWithMapCondition(Map<String, Object> map) {
		return productDao.getNumberOfProductWithMapCondition(map);
	}

	public void calculateListProductFinalPrice(List<Product> productList) {
		for (Product product : productList) {
			calculateProductFinalPrice(product);
		}
	}
	
	public void calculateProductFinalPrice(Product product) {
		double discountPrice = (product.getListPrice().doubleValue() * product.getDiscountPercent()) / 100;
		double finalPrice = product.getListPrice().doubleValue() - discountPrice;
		finalPrice = Math.round(finalPrice * 100.0) / 100.0;
		product.setFinalPrice(finalPrice);
	}
}
