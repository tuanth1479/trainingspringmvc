package com.training.sys.service;

import java.util.List;
import java.util.Map;

import com.training.sys.model.Product;

public interface ProductService {
	List<Product> selectProductList();

	List<Product> selectProductByQueryWithPagination(Map<String, Object> map);

	List<Product> selectProductTopTrend();

	List<Product> selectProductTopDiscount();

	Product selectProductById(int productId);

	Integer getNumberOfProductWithMapCondition(Map<String, Object> map);

	void calculateListProductFinalPrice(List<Product> productList);

	void calculateProductFinalPrice(Product product);
}
