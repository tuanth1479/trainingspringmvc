package com.training.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.sys.dao.CompanyInfoDao;
import com.training.sys.model.CompanyInfo;

@Service
public class CompanyInfoServiceImpl implements CompanyInfoService{

	@Autowired
	private CompanyInfoDao companyInfoDao;
	
	public CompanyInfo getCompanyInfo() {
		return companyInfoDao.getCompanyInfo();
	}
}
