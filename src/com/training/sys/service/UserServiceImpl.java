package com.training.sys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.training.sys.dao.UserDao;
import com.training.sys.model.User;
import com.training.sys.model.UserPrincipal;

@Service("UserService")
public class UserServiceImpl implements UserService, UserDetailsService {
	@Autowired
	private UserDao userDao;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Transactional(readOnly = true)
	public List<User> selectMany() {
		return userDao.selectMany();
	}

	@Transactional
	public void addUser(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userDao.addUser(user);
	}

	@Transactional(readOnly = true)
	public User selectUserById(int id) {
		return userDao.selectUserById(id);
	}

	public User selectUserByEmai(String email) {
		return userDao.selectUserByEmail(email);
	}
	
	@Transactional
	public void updateUser(User user) {
		userDao.updateUser(user);
	}
	
	@Transactional
	public void updateAvatarUser(User user) {
		userDao.updateAvatarUser(user);
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = userDao.selectUserByEmail(email);

		if (user == null) {
			return null;
		}
		return (UserDetails) new UserPrincipal(user);
	}

	
	
}
