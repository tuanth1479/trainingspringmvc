package com.training.sys.service;

import com.training.sys.model.CompanyInfo;

public interface CompanyInfoService {
	CompanyInfo getCompanyInfo();
}
