package com.training.sys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.sys.dao.CartDetailDao;
import com.training.sys.model.CartDetail;

@Service
public class CartDetailServiceImpl implements CartDetailService {
	@Autowired
	CartDetailDao cartDetailDao;

	public List<CartDetail> selectAllProductInCart(int cartId) {
		return cartDetailDao.selectAllProductInCart(cartId);
	}

	public void updateProductInCart(CartDetail cartDetail) {
		cartDetailDao.updateProductInCart(cartDetail);
	}

	public void addProductToCart(CartDetail cartDetail) {
		cartDetailDao.addProductToCart(cartDetail);
	}
	
	public boolean checkProductInCart(CartDetail cartDetail) {
		CartDetail tempCartDetail = cartDetailDao.selectOneProductInCart(cartDetail);
		if(tempCartDetail == null)
			return false;
		
		return true;
	}
	
	public CartDetail selectOneProductInCart(CartDetail cartDetail) {
		return cartDetailDao.selectOneProductInCart(cartDetail);
	}
	
	public void deleteProductFromCart(CartDetail cartDetail) {
		cartDetailDao.deleteProductFromCart(cartDetail);
	}
}
