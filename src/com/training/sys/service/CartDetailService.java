package com.training.sys.service;

import java.util.List;

import com.training.sys.model.CartDetail;

public interface CartDetailService {
	List<CartDetail> selectAllProductInCart(int cartId);

	boolean checkProductInCart(CartDetail cartDetail);

	CartDetail selectOneProductInCart(CartDetail cartDetail);

	void addProductToCart(CartDetail cartDetail);

	void updateProductInCart(CartDetail cartDetail);
	
	void deleteProductFromCart(CartDetail cartDetail);
}
