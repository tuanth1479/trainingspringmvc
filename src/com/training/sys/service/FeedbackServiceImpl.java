package com.training.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.sys.dao.FeedbackDao;
import com.training.sys.model.Feedback;

@Service
public class FeedbackServiceImpl implements FeedbackService {
	@Autowired
	FeedbackDao feedbackDao;

	public void addFeedback(Feedback feedback) {
		feedbackDao.addFeedback(feedback);
	}
}
