package com.training.sys.service;

import java.util.List;

import com.training.sys.model.User;

public interface UserService {
	List<User> selectMany();

	void addUser(User user);

	User selectUserById(int id);

	User selectUserByEmai(String email);

	void updateUser(User user);
	
	void updateAvatarUser(User user);
}
