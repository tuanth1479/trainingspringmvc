package com.training.sys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.sys.dao.OrderDao;
import com.training.sys.model.Order;

@Service
public class OrderServiceImpl implements OrderService {
	@Autowired
	OrderDao orderDao;
	@Autowired
	CartService cartService;

	public List<Order> selectAllOrderByUserId(int userId){
		return orderDao.selectAllOrderByUserId(userId);
	}

	public void cancelOrderById(int orderId) {
		orderDao.cancelOrderById(orderId);
	}

	public void createNewOrder(Order order) {
		int cartId = order.getCartId();
		cartService.deactiveCart(cartId);
		
		orderDao.createNewOrder(order);
	}

}
