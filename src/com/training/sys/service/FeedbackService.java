package com.training.sys.service;

import com.training.sys.model.Feedback;

public interface FeedbackService {
	void addFeedback(Feedback feedback);
}
