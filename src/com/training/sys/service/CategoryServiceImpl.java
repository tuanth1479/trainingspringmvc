package com.training.sys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.sys.dao.CategoryDao;
import com.training.sys.model.Category;

@Service
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	CategoryDao categoryDao;
	
	public List<Category> selectCategoryList() {
		return categoryDao.selectCategoryList();
	}
}
