package com.training.sys.service;

import java.util.List;

import com.training.sys.model.Order;

public interface OrderService {
	List<Order> selectAllOrderByUserId(int userId);

	void cancelOrderById(int orderId);

	void createNewOrder(Order order);

}
