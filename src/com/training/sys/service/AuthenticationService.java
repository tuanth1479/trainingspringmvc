package com.training.sys.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

public interface AuthenticationService {
	void checkLoginAndAddUserToModelAttribute(HttpServletRequest request, Model model);
}
