package com.training.sys.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class StorageServiceImpl implements StorageService {
	// Declare Location to save Image
	// Should have a File server to save file and return the path
	private static String UPLOADED_FOLDER = "E:\\Eclipse\\Training\\WebContent\\WEB-INF\\views\\img\\public\\upload";

	public String store(MultipartFile file) {
		try {
			if (file.isEmpty()) {
				return "error_not_select_image";
			}

			// Make directory if not exists
			File dir = new File(UPLOADED_FOLDER);
			System.out.println(dir);
			if (dir.exists() == false) {
				try {
					dir.mkdir();
					return "can_not_save_avatar";
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// Get the file
			Random random = new Random();
			String fileNameSaveToDb = "\\avatar\\" + random.nextInt(999999) + "_" + file.getOriginalFilename();
			// folder to save avatar plus string avatar
			// maybe save other kind of file. Use Switch case to solve.
			byte[] bytes = file.getBytes();
			
			Path path = Paths.get(UPLOADED_FOLDER  + fileNameSaveToDb);

			// Save the file
			Files.write(path, bytes);
			return fileNameSaveToDb;
		} catch (IOException e) {
			e.printStackTrace();
			return "error";
		}
	}

	private static String HOST_ADDRESS = "http://localhost:8080/Training/img/public/upload";

	public String getAccessLinkOfFileWithDatabasePath(String databasePath) {
		return HOST_ADDRESS + databasePath;
	}
}
