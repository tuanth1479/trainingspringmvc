package com.training.sys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.sys.dao.CartDao;
import com.training.sys.model.Cart;
import com.training.sys.model.CartDetail;
import com.training.sys.model.Product;

@Service
public class CartServiceImpl implements CartService {
	@Autowired
	CartDao cartDao;
	@Autowired
	ProductService productService;
	@Autowired
	CartDetailService cartDetailService;

	public Cart selectCartById(int cartId) {
		return cartDao.selectCartById(cartId);
	}
	
	public void deactiveCart(int cartId) {
		cartDao.deactiveCart(cartId);
	}

	public void createCart(Cart cart) {
		cartDao.createCart(cart);
	}

	public Cart selectCartActiveForUser(int userId) {
		return cartDao.selectCartActiveForUser(userId);
	}

	public double calculateTotalCartPrice(List<CartDetail> listCartItem) {
		double total = 0;
		for (CartDetail item : listCartItem) {
			total = total + item.getFinalPrice();
		}
		return Math.round(total * 100.0) / 100.0;
	}

	public void updateInformationForEachProductInCart(int cartId) {
		System.out.println("Update product infor in cart");
		List<CartDetail> listCartItem = cartDetailService.selectAllProductInCart(cartId);

		for (CartDetail cartDetail : listCartItem) {
			int productId = cartDetail.getProductId();
			Product product = productService.selectProductById(productId);
			System.out.println("Product " + product.toString());

			cartDetail.setDiscountPercent(product.getDiscountPercent());
			cartDetail.setListPrice(product.getListPrice());
			cartDetail.setProductName(product.getProductName());

			int quantity = cartDetail.getQuantity();
			double finalPrice = Math.round(product.getFinalPrice() * quantity * 100.0) / 100.0;

			cartDetail.setFinalPrice(finalPrice);

			cartDetailService.updateProductInCart(cartDetail);
		}
	}
}
