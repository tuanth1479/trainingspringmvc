package com.training.sys.service;

import java.util.List;

import com.training.sys.model.Cart;
import com.training.sys.model.CartDetail;

public interface CartService {
	void createCart(Cart cart);
	
	Cart selectCartActiveForUser(int userId);
	
	void updateInformationForEachProductInCart(int cartId);
	
	double calculateTotalCartPrice(List<CartDetail> cartDetailList);
	
	Cart selectCartById(int cartId);
	
	void deactiveCart(int cartId);
}
