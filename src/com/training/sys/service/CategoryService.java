package com.training.sys.service;

import java.util.List;

import com.training.sys.model.Category;

public interface CategoryService {
	List<Category> selectCategoryList();
}
