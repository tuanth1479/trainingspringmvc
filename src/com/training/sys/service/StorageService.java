package com.training.sys.service;

import org.springframework.web.multipart.MultipartFile;

public interface StorageService {
	String store(MultipartFile file);

	String getAccessLinkOfFileWithDatabasePath(String databasePath);
}
