package com.training.sys.service;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class AuthenticationServiceImpl implements AuthenticationService{
	public void checkLoginAndAddUserToModelAttribute(HttpServletRequest request, Model model) {
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal != null) {
			System.out.println("userPrin " + userPrincipal.toString());
			model.addAttribute("user", userPrincipal);
		}
	}
	
}
