package com.training.sys.model;

import java.math.BigDecimal;

public class Product {
	private int productId;
	private int catId;
	private String productName;
	private String productImage;
	private BigDecimal listPrice;
	private int discountPercent;
	private double finalPrice;
	private boolean isTrend;

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", catId=" + catId + ", productName=" + productName
				+ ", productImage=" + productImage + ", listPrice=" + listPrice + ", discountPercent=" + discountPercent
				+ ", finalPrice=" + finalPrice + ", isTrend=" + isTrend + "]";
	}

	public boolean isTrend() {
		return isTrend;
	}

	public void setTrend(boolean isTrend) {
		this.isTrend = isTrend;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getCatId() {
		return catId;
	}

	public void setCatId(int catId) {
		this.catId = catId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getListPrice() {
		return listPrice;
	}

	public void setListPrice(BigDecimal listPrice) {
		this.listPrice = listPrice;
	}

	public int getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(int discountPercent) {
		this.discountPercent = discountPercent;
	}

}
