package com.training.sys.model;

import java.math.BigDecimal;

public class CartDetail {
	private int cartId;
	private int productId;
	private String productName;
	private int quantity;
	private BigDecimal listPrice;
	private double finalPrice;
	private int discountPercent;

	@Override
	public String toString() {
		return "CartDetail [cartId=" + cartId + ", productId=" + productId + ", productName=" + productName
				+ ", quantity=" + quantity + ", listPrice=" + listPrice + ", finalPrice=" + finalPrice
				+ ", discountPercent=" + discountPercent + "]";
	}

	public double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getListPrice() {
		return listPrice;
	}

	public void setListPrice(BigDecimal listPrice) {
		this.listPrice = listPrice;
	}

	public int getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(int discountPercent) {
		this.discountPercent = discountPercent;
	}

}
