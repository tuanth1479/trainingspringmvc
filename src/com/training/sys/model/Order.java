package com.training.sys.model;

import java.sql.Timestamp;

public class Order {
	private int orderId;
	private int cartId;
	private int userId;
	private double orderPrice;
	private boolean orderStatus;
	private Timestamp createdAt;
	private Timestamp updatedAt;

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", cartId=" + cartId + ", userId=" + userId + ", orderPrice=" + orderPrice
				+ ", orderStatus=" + orderStatus + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public double getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(double orderPrice) {
		this.orderPrice = orderPrice;
	}

	public boolean isOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(boolean orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}
