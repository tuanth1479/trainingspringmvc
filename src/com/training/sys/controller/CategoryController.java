package com.training.sys.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.training.sys.service.CategoryService;

@Controller
public class CategoryController {
	@Autowired
	CategoryService categoryService;

	//test
	@GetMapping("/cat/list")
	public String getCategoryList(Model model) {
		try {
			model.addAttribute("errors", categoryService.selectCategoryList().toString());
			return "errorPage";
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}

	}
}
