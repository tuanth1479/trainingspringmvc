package com.training.sys.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.training.sys.service.AuthenticationService;
import com.training.sys.service.CategoryService;
import com.training.sys.service.ProductService;
import com.training.sys.service.StorageService;

@Controller
public class HomeController {
	@Autowired
	CategoryService categoryService;
	@Autowired
	ProductService productService;
	@Autowired
	AuthenticationService authenticationService;
	@Autowired
	StorageService storageService;

	@GetMapping("/")
	public String index(Model model, HttpServletRequest request) {
		try {
			authenticationService.checkLoginAndAddUserToModelAttribute(request, model);

			model.addAttribute("catList", categoryService.selectCategoryList());
			model.addAttribute("topTrend", productService.selectProductTopTrend());
			model.addAttribute("listProduct", productService.selectProductTopDiscount());
			return "index";
		} catch (Exception e) {
			model.addAttribute("error", e.toString());
			return "errorPage";
		}
	}

	@GetMapping("/errorPage")
	public String getViewError(Model model) {

		try {
			model.addAttribute("error", "Sorry, Something went wrong!");
			return "errorPage";
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}

	}

	@GetMapping("/upload")
	public String getViewupload(Model model) {
		return "upload";
	}

	@PostMapping("/upload")
	public String singleFileUpload(@RequestParam("file") MultipartFile file, Model model) {

		try {
			String message = null;
			message = storageService.store(file);
			model.addAttribute("message", message);
			return "uploadStatus";
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}
	}
}
