package com.training.sys.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.training.sys.model.Product;
import com.training.sys.service.AuthenticationService;
import com.training.sys.service.CategoryService;
import com.training.sys.service.ProductService;

@Controller
public class ProductController {
	@Autowired
	ProductService productService;
	@Autowired
	CategoryService categoryService;
	@Autowired
	AuthenticationService authenticationService;

	@GetMapping("/product/{id}")
	public String getViewProductDetailById(Model model, @PathVariable("id") int id, HttpServletRequest request) {
		try {
			authenticationService.checkLoginAndAddUserToModelAttribute(request, model);

			model.addAttribute("product", productService.selectProductById(id));
			System.out.println("product " + productService.selectProductById(id));
			return "productDetail";
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}
	}

	@GetMapping("/product/list")
	public String getProductListByQuery(Model model, HttpServletRequest request) {
		try {
			authenticationService.checkLoginAndAddUserToModelAttribute(request, model);

			Map<String, Object> map = new HashMap<>();
			List<Product> productList = new ArrayList<>();

			int pageSize = 12;
			map.put("pageSize", pageSize);

			Integer numOfProduct = 0;
			int numOfPage = 0;

			String catIdQueryParam = request.getParameter("catId");
			System.out.println("catIdQueryParam " + catIdQueryParam);
			if (catIdQueryParam == null) {
				catIdQueryParam = "0";
			}

			String pageQueryParam = request.getParameter("page");
			System.out.println("pageQueryParam " + pageQueryParam);
			if (pageQueryParam == null) {
				pageQueryParam = "1";
			}

			System.out.println("catIdQueryParamFixed " + catIdQueryParam);
			System.out.println("pageQueryParamFixed " + pageQueryParam);

			int catId = 0;
			catId = Integer.parseInt(catIdQueryParam);

			int currentPage = 1;
			currentPage = Integer.parseInt(pageQueryParam);
			if (currentPage <= 0) {
				currentPage = 1;
			}

//			offset = 0 12 24 36 ...
			int offset = (currentPage - 1) * pageSize;
			if (offset <= 0) {
				map.put("offset", 0);
			} else {
				map.put("offset", offset);
			}

			System.out.println("Page " + currentPage);
			System.out.println("offset " + offset);
			System.out.println("catID " + catId);

			if (catId <= 0) {
				// filter by search string or return default TopDiscount products
				String searchQueryParam = request.getParameter("q");

				if (searchQueryParam == null) {
					model.addAttribute("listProduct", productService.selectProductTopDiscount());
				} else {
					searchQueryParam = '%' + searchQueryParam + '%';
					map.put("query", searchQueryParam);
					numOfProduct = productService.getNumberOfProductWithMapCondition(map);
					numOfPage = (numOfProduct / pageSize) + 1;
					productList = productService.selectProductByQueryWithPagination(map);
					addListNumberOfPageToModel(numOfPage, currentPage, model); // Calculate and Show 1 2 3 page number
																				// on the web site

					model.addAttribute("listProduct", productList);
					model.addAttribute("numOfPage", numOfPage);
				}

			} else { // filter by catID
				map.put("catId", catId);
				// completed map condition before get number of product

				numOfProduct = productService.getNumberOfProductWithMapCondition(map);
				numOfPage = (numOfProduct / pageSize) + 1;
				productList = productService.selectProductByQueryWithPagination(map);
				addListNumberOfPageToModel(numOfPage, currentPage, model); // Calculate and Show 1 2 3 page number on
																			// the web site

				System.out.println("numOfProduct " + numOfProduct);
				System.out.println("numOfPage " + numOfPage);

				model.addAttribute("listProduct", productList);
				model.addAttribute("numOfPage", numOfPage);

				model.addAttribute("catId", catId);
//				test output
//				model.addAttribute("errors", productService.selectProductByQuery(map));
//				return "errorPage";
			}

			model.addAttribute("catList", categoryService.selectCategoryList());
			model.addAttribute("topTrend", productService.selectProductTopTrend());

			return "index";

		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}

	}

	public void addListNumberOfPageToModel(int numOfPage, int currentPage, Model model) {
		List<Integer> numberOfPageList = new ArrayList<>();
		if (numOfPage <= 4) { // 1 2 3
			for (int i = 1; i <= numOfPage; i++) {
				numberOfPageList.add(i);
			}
		} else { // 4 5 6 7...
			if (currentPage == 1) {
				numberOfPageList.add(1);
				numberOfPageList.add(2);
				numberOfPageList.add(3);
				numberOfPageList.add(4);
			} else {
				for (int i = currentPage - 1; i <= currentPage + 2; i++) {

					if (i <= numOfPage) {
						numberOfPageList.add(i);
					}

				}
			}
		}
		model.addAttribute("numberOfPageList", numberOfPageList);
	}

}
