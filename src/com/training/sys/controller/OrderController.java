package com.training.sys.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.training.sys.model.Cart;
import com.training.sys.model.CartDetail;
import com.training.sys.model.Order;
import com.training.sys.model.User;
import com.training.sys.service.AuthenticationService;
import com.training.sys.service.CartDetailService;
import com.training.sys.service.CartService;
import com.training.sys.service.OrderService;
import com.training.sys.service.UserService;

@Controller
public class OrderController {
	@Autowired
	OrderService orderService;
	@Autowired
	UserService userService;
	@Autowired
	CartService cartService;
	@Autowired
	CartDetailService cartDetailService;
	@Autowired
	AuthenticationService authenticationService;

	@GetMapping("/order")
	public String getViewOrderHistory(Model model, HttpServletRequest request) {

		try {
			authenticationService.checkLoginAndAddUserToModelAttribute(request, model);
			// Get user
			int userId = 0;
			Principal userPrincipal = request.getUserPrincipal();
			String email = userPrincipal.getName();
			User user = userService.selectUserByEmai(email);
			userId = user.getId();
			System.out.println("userId " + userId);

			List<Order> orderHistoryList = orderService.selectAllOrderByUserId(userId);
			System.out.println("List " + orderHistoryList);
			model.addAttribute("orderHistoryList", orderHistoryList);
			return "orderHistory";

		} catch (Exception e) {
			model.addAttribute("error", e.toString());
			return "errorPage";
		}
	}

	@PostMapping("/order/create")
	public String createOrder(Model model, HttpServletRequest request) {
		try {
			// Get user
			int userId = 0;
			Principal userPrincipal = request.getUserPrincipal();
			String email = userPrincipal.getName();
			User user = userService.selectUserByEmai(email);
			userId = user.getId();
			System.out.println("userId " + userId);

			// Get cart
			Cart cart = cartService.selectCartActiveForUser(userId);
			int cartId = cart.getCartId();

			// Update cart info
			cartService.updateInformationForEachProductInCart(cartId);

			// Tinh gia total cua don hang sau khi cap nhat
			List<CartDetail> listProductInCart = cartDetailService.selectAllProductInCart(cartId);
			if (listProductInCart.size() == 0) {
				model.addAttribute("error", "Chua co do trong gio hang. Mua gi di roi thanh toan nha!");
				return "errorPage";
			}
			double totalPrice = cartService.calculateTotalCartPrice(listProductInCart);

			// create new order
			Order order = new Order();
			order.setCartId(cartId);
			order.setUserId(userId);
			order.setOrderPrice(totalPrice);
			orderService.createNewOrder(order);
			return "redirect:/order";
		} catch (Exception e) {
			model.addAttribute("error", e.toString());
			return "errorPage";
		}
	}

	@PostMapping("/order/cancel")
	public String cancelOrder(HttpServletRequest request, Model model) {
		try {
			String orderIdParam = request.getParameter("orderId");
			int orderId = 0;
			if (orderIdParam != null) {
				orderId = Integer.parseInt(orderIdParam);
			}

			orderService.cancelOrderById(orderId);

			return "redirect:/order";
		} catch (Exception e) {
			model.addAttribute("error", e.toString());
			return "errorPage";
		}

	}

}
