package com.training.sys.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.training.sys.model.Cart;
import com.training.sys.model.CartDetail;
import com.training.sys.model.Product;
import com.training.sys.model.User;
import com.training.sys.service.AuthenticationService;
import com.training.sys.service.CartDetailService;
import com.training.sys.service.CartService;
import com.training.sys.service.ProductService;
import com.training.sys.service.UserService;

@Controller
public class CartController {
	@Autowired
	CartService cartService;
	@Autowired
	CartDetailService cartDetailService;
	@Autowired
	UserService userService;
	@Autowired
	ProductService productService;
	@Autowired
	AuthenticationService authenticationService;

	@GetMapping("/cart")
	public String getCartView(Model model, HttpServletRequest request) {
		try {
			authenticationService.checkLoginAndAddUserToModelAttribute(request, model);
//			Get user
			int userId = 0;
			Principal userPrincipal = request.getUserPrincipal();
			String email = userPrincipal.getName();
			User user = userService.selectUserByEmai(email);
			userId = user.getId();
			System.out.println("userId " + userId);

//			Khoi tao cartId = 0
			int cartId = 0;
			Cart cart = cartService.selectCartActiveForUser(userId);

			if (cart == null) {
				Cart tempCart = new Cart();
				tempCart.setUserId(userId);
				cartService.createCart(tempCart); // Tao moi cart cho userId

				cart = cartService.selectCartActiveForUser(userId);
				cartId = cart.getCartId();
			} else {
				cartId = cart.getCartId();
			}

			System.out.println("cartId " + cartId);

			// function update information (price + discount) for all product in cart
			// param: list cart
			cartService.updateInformationForEachProductInCart(cartId);

			List<CartDetail> listCartItem = cartDetailService.selectAllProductInCart(cartId);
			// calculate total price when update was completed

			model.addAttribute("totalPrice", cartService.calculateTotalCartPrice(listCartItem));
			model.addAttribute("listCartItem", listCartItem);
			return "cartDetail";
		} catch (Exception e) {
			model.addAttribute("error", e.toString());
			return "errorPage";
		}
	}

	@PostMapping("/cart")
	public String addAndUpdateProductToCart(Model model, HttpServletRequest request) {

		try {
			// get product
			String productIdRequestParam = request.getParameter("productId");
			int productId = 0;
			if (productIdRequestParam != null) {
				productId = Integer.parseInt(productIdRequestParam);
			}
			System.out.println("productId " + productId);
			Product product = productService.selectProductById(productId);
			productService.calculateProductFinalPrice(product); // update final price for sure

			// get user
			int userId = 0;
			Principal userPrincipal = request.getUserPrincipal();
			String email = userPrincipal.getName();
			User user = userService.selectUserByEmai(email);
			userId = user.getId();
			System.out.println("userId " + userId);

			// Khoi tao cart hoac get cartId
			int cartId = 0;
			Cart cart = cartService.selectCartActiveForUser(userId);
			if (cart == null) {
				Cart tempCart = new Cart();
				tempCart.setUserId(userId);
				cartService.createCart(tempCart); // Tao moi cart cho userId

				cart = cartService.selectCartActiveForUser(userId);
				cartId = cart.getCartId();
			} else {
				cartId = cart.getCartId();
			}

			// Check cartdetail
			CartDetail cartDetail = new CartDetail();
			cartDetail.setCartId(cartId);
			cartDetail.setProductId(productId);

			// main
			copyProductInfoAndSetQuantityToCart(product, cartDetail, request);

			return "redirect:/cart";
		} catch (Exception e) {
			model.addAttribute("error", e.toString());
			return "errorPage";
		}
	}

	@PostMapping("/cart/delete")
	public String deleteProductInCart(HttpServletRequest request, Model model) {

		try {
			// get product
			String productIdRequestParam = request.getParameter("productId");
			int productId = 0;
			if (productIdRequestParam != null) {
				productId = Integer.parseInt(productIdRequestParam);
			}
			System.out.println("productId " + productId);
			Product product = productService.selectProductById(productId);
			productService.calculateProductFinalPrice(product); // update final price for sure

			// get user
			int userId = 0;
			Principal userPrincipal = request.getUserPrincipal();
			String email = userPrincipal.getName();
			User user = userService.selectUserByEmai(email);
			userId = user.getId();
			System.out.println("userId " + userId);

			// get user's cart
			int cartId = 0;
			Cart cart = cartService.selectCartActiveForUser(userId);
			cartId = cart.getCartId();

			// Create cartDetail with cartId and productId
			CartDetail cartDetail = new CartDetail();
			cartDetail.setCartId(cartId);
			cartDetail.setProductId(productId);

			cartDetailService.deleteProductFromCart(cartDetail);

			System.out.println("Delete successfully product in cart " + cartId);
			return "redirect:/cart";
		} catch (Exception e) {
			model.addAttribute("error", e.toString());
			return "errorPage";
		}
	}

	void copyProductInfoAndSetQuantityToCart(Product product, CartDetail cartDetail, HttpServletRequest request) {
		int quantity = 0;
		if (cartDetailService.checkProductInCart(cartDetail)) { // da ton tai product in cart => update
			cartDetail = cartDetailService.selectOneProductInCart(cartDetail);

			// set quantity
			String quantityParam = request.getParameter("quantity");
			if (quantityParam != null) {
				quantity = Integer.parseInt(quantityParam);
			} else {
				quantity = cartDetail.getQuantity() + 1;
			}

			// update thong tin product in cart
			cartDetail.setQuantity(quantity);
			cartDetail.setProductName(product.getProductName());
			cartDetail.setListPrice(product.getListPrice());
			cartDetail.setDiscountPercent(product.getDiscountPercent());

			double finalPrice = Math.round(product.getFinalPrice() * quantity * 100.0) / 100.0;

			cartDetail.setFinalPrice(finalPrice);

			// final
			cartDetailService.updateProductInCart(cartDetail);
		} else { // chua ton tai product in cart

			// update thong tin product in cart
			cartDetail.setQuantity(1); // quantity = 1
			cartDetail.setProductName(product.getProductName());
			cartDetail.setListPrice(product.getListPrice());
			cartDetail.setDiscountPercent(product.getDiscountPercent());

			// quantity = 1
			double productFinalPrice = Math.round(product.getFinalPrice() * 100.0) / 100.0;
			cartDetail.setFinalPrice(productFinalPrice);

			// final
			cartDetailService.addProductToCart(cartDetail);
		}
	}
}
