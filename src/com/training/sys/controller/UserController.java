package com.training.sys.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.training.sys.model.User;
import com.training.sys.service.AuthenticationService;
import com.training.sys.service.StorageService;
import com.training.sys.service.UserService;

@Controller
public class UserController {
//	private final String UPLOAD_DIR = "./views/img/public/upload/avatar";

	@Autowired
	private UserService userService;
	@Autowired
	AuthenticationService authenticationService;
	@Autowired 
	StorageService storageService;

	@GetMapping("/user/login")
	public String getLoginView(HttpServletRequest request, User user, Model model) {
		try {
			HttpSession session = request.getSession();
			Principal userPrincipal = request.getUserPrincipal();
			System.out.println("----------------- Session User is " + session.getId());
			System.out.println("----------------- User Principal is " + userPrincipal);
			if (userPrincipal != null) {
				return "redirect:/";
			} else {
				session.invalidate();
				return "login";
			}
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}

	}

	@GetMapping("/user/list")
	public String getUserList(ModelMap model) {
		try {
			List<User> listUsers = userService.selectMany();
			model.addAttribute("userList", listUsers);
			return "listUser";
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}

	}

	@GetMapping("/user/register")
	public String getUserRegisterView(User user, Model model) {
		try {
			return "registerUser";
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}
	}

	@PostMapping("/user/register")
	public String addUser(User user, Model model, HttpServletRequest request) {
		try {
			String email = request.getParameter("email");
			if (email.equalsIgnoreCase("")) {
				model.addAttribute("error", "Email field is empty.");
				return "errorPage";
			} else {
				User existUser = userService.selectUserByEmai(email);
				if (existUser != null) {
					model.addAttribute("error", "User Exist.");
					return "errorPage";
				} else {
					user.setAvatar("/avatar/avatar_default.jpg");
					userService.addUser(user);
					return "redirect:/user/login";
				}
			}
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}
	}

	@GetMapping("/user/profile")
	public String getUserProfileView(HttpServletRequest request, Model model) {
		try {
			authenticationService.checkLoginAndAddUserToModelAttribute(request, model);
			HttpSession session = request.getSession();
			Principal userPrincipal = request.getUserPrincipal();
			System.out.println("----------------- Session User is " + session.getId());
			System.out.println("----------------- User Principal is " + userPrincipal);
			System.out.println("----------------- User Name Principal is " + userPrincipal.getName()); // email is
			
			String email = userPrincipal.getName();
			User selectUser = userService.selectUserByEmai(email);
			
			return "redirect:/user/" + selectUser.getId();
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}

	}

	@GetMapping("/user/{id}")
	public String getUserProfile(User user, @PathVariable("id") int id, Model model, HttpServletRequest request) {
		try {
			authenticationService.checkLoginAndAddUserToModelAttribute(request, model);

			User userPrincipal = userService.selectUserById(id);
			if (userPrincipal.getId() != id) {
				model.addAttribute("error", "Can not access profile of other user");
				return "errorPage";
			} else {
				User selectUser = userService.selectUserById(id);
				if (selectUser == null) {
					String errorString = "User not found";
					model.addAttribute("error", errorString);
					return "errorPage";
				}

				String avatarPath = selectUser.getAvatar();
				String fullLink = storageService.getAccessLinkOfFileWithDatabasePath(avatarPath);
				
				model.addAttribute("avatar", fullLink);
				model.addAttribute("user", selectUser);
				return "profileUser";
			}
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}
	}

	@PostMapping("/user/{id}")
	public String updateUser(User user, Model model, HttpServletRequest request, @PathVariable("id") int id) {
		try {
			String email = request.getParameter("email");
			System.out.println("userId = " + id);
			if (email.equalsIgnoreCase("")) {
				model.addAttribute("error", "Email field is empty.");
				return "errorPage";
			} else {
				User existUser = userService.selectUserByEmai(email);
				if (existUser != null) {
					if (existUser.getId() != id) {
						model.addAttribute("error", "User Exist.");
						return "errorPage";
					} else {
						System.out.println("user info: " + user.toString());
						userService.updateUser(user);
						return "redirect:/user/" + id;
					}
				} else {
					System.out.println("user info: " + user.toString());
					userService.updateUser(user);

					System.out.println("Edit done ! Redirect user/id");
					return "redirect:/user/" + id;
				}
			}
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}

	}
	
	@PostMapping("/user/change-avatar")
	public String singleFileUpload(@RequestParam("file") MultipartFile file, Model model, HttpServletRequest request) {
		try {
			Principal principal = request.getUserPrincipal();
			
			User user = userService.selectUserByEmai(principal.getName());
			
			String path = null;
			path = storageService.store(file);
			if ("error_not_select_image".equals(path)) {
				model.addAttribute("error", "Sorry, selectFile");
				return "errorPage";
			}
			
			if ("error".equals(path)) {
				model.addAttribute("error", "Sorry, something went wrong!");
				return "errorPage";
			}
			
			if ("can_not_save_avatar".equals(path)) {
				model.addAttribute("error", "Sorry, can not save your avatar to server! You can try to config the right path to save avatar in package service/StorageServiceIml, static variable UPLOADED_FOLDER");
				return "errorPage";
			}
			
			user.setAvatar(path);
			userService.updateAvatarUser(user);
			return "redirect:/user/profile";
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}
	}

	@PostMapping("/logout")
	public String logout(HttpServletRequest request, Model model) {
		try {
			HttpSession session = request.getSession();
			session.invalidate();
			return "redirect:/login";
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}

	}
}
