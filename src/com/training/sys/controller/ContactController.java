package com.training.sys.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.training.sys.model.Feedback;
import com.training.sys.service.CompanyInfoService;
import com.training.sys.service.FeedbackService;

@Controller
public class ContactController {
	@Autowired
	CompanyInfoService companyInfoService;
	@Autowired
	FeedbackService feedbackService;

	@GetMapping("/contact")
	public String getCompanyInfo(Feedback feedback, Model model) {
		try {
			model.addAttribute("companyInfo", companyInfoService.getCompanyInfo());
			return "contact";
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}
	}

	@PostMapping("/feedback")
	public String addUser(Feedback feedback, Model model) {
		try {
			feedbackService.addFeedback(feedback);
			return "redirect:contact";
		} catch (Exception e) {
			model.addAttribute("error", "Sorry, something went wrong!");
			return "errorPage";
		}
	}
}
