package com.training.sys.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.training.sys.model.Cart;

@Repository
public class CartDaoImpl implements CartDao {
	@Autowired
	SqlSession sqlSession;

	public void createCart(Cart cart) {
		sqlSession.insert("CartMapper.createCart", cart);
	}

	public Cart selectCartActiveForUser(int userId) {
		return sqlSession.selectOne("CartMapper.selectCartActiveForUser", userId);
	}

	public Cart selectCartById(int cartId) {
		return sqlSession.selectOne("CartMapper.selectCartById", cartId);
	}

	public void deactiveCart(int cartId) {
		sqlSession.update("CartMapper.deactiveCart", cartId);
	}
}
