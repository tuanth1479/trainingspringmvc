package com.training.sys.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.training.sys.model.Product;

@Repository
public class ProductDaoImpl implements ProductDao {

	@Autowired
	SqlSession sqlSession;

	public List<Product> selectProductList() {
		return sqlSession.selectList("ProductMapper.selectProductList");
	}

	public List<Product> selectProductTopTrend() {
		return sqlSession.selectList("ProductMapper.selectProductTopTrend");

	}

	public List<Product> selectProductTopDiscount() {
		return sqlSession.selectList("ProductMapper.selectProductTopDiscount");
	}

	public List<Product> selectProductByQueryWithPagination(Map<String, Object> map) {
		return sqlSession.selectList("ProductMapper.selectProductByQueryWithPagination", map);
	}

	public Product selectProductById(int id) {
		return sqlSession.selectOne("ProductMapper.selectProductById", id);
	}

	public Integer getNumberOfProductWithMapCondition(Map<String, Object> map) {
		return sqlSession.selectOne("ProductMapper.getNumberOfProductWithMapCondition", map);
	}
}
