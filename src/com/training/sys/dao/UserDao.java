package com.training.sys.dao;

import java.util.List;

import com.training.sys.model.User;

public interface UserDao {
	List<User> selectMany();

	void addUser(User user);

	User selectUserById(int id);

	User selectUserByEmail(String email);

	void updateUser(User user);
	
	void updateAvatarUser(User user);
}
