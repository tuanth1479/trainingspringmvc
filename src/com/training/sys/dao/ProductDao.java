package com.training.sys.dao;

import java.util.List;
import java.util.Map;

import com.training.sys.model.Product;

public interface ProductDao {
	List<Product> selectProductList();

	List<Product> selectProductByQueryWithPagination(Map<String, Object> map);

	Integer getNumberOfProductWithMapCondition(Map<String, Object> map);

	List<Product> selectProductTopTrend();

	List<Product> selectProductTopDiscount();

	Product selectProductById(int id);
}
