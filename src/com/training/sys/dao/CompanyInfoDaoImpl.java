package com.training.sys.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.training.sys.model.CompanyInfo;

@Repository
public class CompanyInfoDaoImpl implements CompanyInfoDao {
	@Autowired
	SqlSession sqlSession;

	public CompanyInfo getCompanyInfo() {
		return sqlSession.selectOne("CompanyInfoMapper.getCompanyInfo");
	}
}
