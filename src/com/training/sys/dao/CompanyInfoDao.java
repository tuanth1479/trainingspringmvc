package com.training.sys.dao;

import com.training.sys.model.CompanyInfo;

public interface CompanyInfoDao {
	CompanyInfo getCompanyInfo();

}
