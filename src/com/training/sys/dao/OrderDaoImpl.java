package com.training.sys.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.training.sys.model.Order;

@Repository
public class OrderDaoImpl implements OrderDao {
	@Autowired 
	SqlSession sqlSession;
	
	public List<Order> selectAllOrderByUserId(int userId){
		return sqlSession.selectList("OrderMapper.selectAllOrderByUserId", userId);
	}
	
	public void cancelOrderById(int orderId) {
		sqlSession.update("OrderMapper.cancelOrderById", orderId);
	}
	
	public void createNewOrder(Order order) {
		sqlSession.insert("OrderMapper.createNewOrder", order);
	}
}
