package com.training.sys.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.training.sys.model.User;

@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	private SqlSession sqlSession;

	@Override
	public List<User> selectMany() {
		return sqlSession.selectList("UserMapper.selectMany");
	}

	public void addUser(User user) {
		sqlSession.insert("UserMapper.addUser", user);
	}
	
	public User selectUserById (int id) {
		return sqlSession.selectOne("UserMapper.selectUserById", id);
	}
	
	public User selectUserByEmail(String email) {
		return sqlSession.selectOne("UserMapper.selectUserByEmail", email);
	}
	
	public void updateUser(User user) {
		sqlSession.update("UserMapper.updateUser", user);
	}
	
	public void updateAvatarUser(User user) {
		sqlSession.update("UserMapper.updateAvatarUser", user);
	}
}
