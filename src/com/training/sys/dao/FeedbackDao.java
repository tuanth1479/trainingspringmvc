package com.training.sys.dao;

import com.training.sys.model.Feedback;

public interface FeedbackDao {
	public void addFeedback(Feedback feedback);
}
