package com.training.sys.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.training.sys.model.Category;

@Repository
public class CategoryDaoIml implements CategoryDao{
	@Autowired SqlSession sqlSession;
	public List<Category> selectCategoryList() {
		return sqlSession.selectList("CategoryMapper.selectCategoryList");
	}
}
