package com.training.sys.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.training.sys.model.Feedback;

@Repository
public class FeedbackDaoImpl implements FeedbackDao {
	@Autowired
	SqlSession sqlSession;

	public void addFeedback(Feedback feedback) {
		sqlSession.insert("FeedbackMapper.addFeedback", feedback);
	}
}
