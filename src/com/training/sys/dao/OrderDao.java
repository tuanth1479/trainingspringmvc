package com.training.sys.dao;

import java.util.List;

import com.training.sys.model.Order;

public interface OrderDao {
	List<Order> selectAllOrderByUserId(int userId);
	
	void cancelOrderById(int orderId);
	
	void createNewOrder(Order order);

}
