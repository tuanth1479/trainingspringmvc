package com.training.sys.dao;

import java.util.List;

import com.training.sys.model.CartDetail;

public interface CartDetailDao {
	List<CartDetail> selectAllProductInCart(int cartId);
	
	CartDetail selectOneProductInCart(CartDetail cartDetail);
	
	void addProductToCart(CartDetail cartDetail);

	void updateProductInCart(CartDetail cartDetail);

	void deleteProductFromCart(CartDetail cartDetail);
}
