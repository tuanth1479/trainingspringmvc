package com.training.sys.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.training.sys.model.CartDetail;

@Repository
public class CartDetailDaoImpl implements CartDetailDao {
	@Autowired
	SqlSession sqlSession;

	public List<CartDetail> selectAllProductInCart(int cartId) {
		return sqlSession.selectList("CartDetailMapper.selectAllProductInCart", cartId);
	}

	public void updateProductInCart(CartDetail cartDetail) {
		System.out.println("detail dao " + cartDetail);
		sqlSession.update("CartDetailMapper.updateProductInCart", cartDetail);
	}

	public void addProductToCart(CartDetail cartDetail) {
		sqlSession.insert("CartDetailMapper.addProductToCart", cartDetail);

	}

	public CartDetail selectOneProductInCart(CartDetail cartDetail) {
		return sqlSession.selectOne("CartDetailMapper.selectOneProductInCart", cartDetail);
	}
	
	public void deleteProductFromCart(CartDetail cartDetail) {
		sqlSession.delete("CartDetailMapper.deleteProductFromCart", cartDetail);		
	}
}
