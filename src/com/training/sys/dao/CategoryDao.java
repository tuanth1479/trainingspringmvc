package com.training.sys.dao;

import java.util.List;

import com.training.sys.model.Category;

public interface CategoryDao {
	List<Category> selectCategoryList();
	
}
