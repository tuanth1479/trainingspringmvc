package com.training.sys.dao;

import com.training.sys.model.Cart;

public interface CartDao {
	void createCart(Cart cart);

	Cart selectCartActiveForUser(int userId);
	
	Cart selectCartById(int cartId);
	
	void deactiveCart(int cartId);
}
