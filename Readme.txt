Repository Bitbucker URL: https://tuanth1479@bitbucket.org/tuanth1479/trainingspringmvc/src/master/
Clone: git clone https://tuanth1479@bitbucket.org/tuanth1479/trainingspringmvc.git

Status: Good, All done.
TODO:
 - Optimize code, UI/UX

Account Login:
admin@mail / admin

Set up:
	1- MySQL: dbName: test - dbPassword: 1234 -> run script "TranHoangTuan_S11_DB.sql"
	2- Need Java JDK 1.8.0_251
	3- Need Tomcat server v9.0
	4- Run project on Tomcat Server v9
	5- Open "http://localhost:8080/Training" by Chrome
	6- If cannot change avatar, please check StorageService, static variable UPLOAD_FOLDER: Config location to save file + Config base_host address to return 
	the link of file.

Email: tuanth.nc.uit@gmail.com
Skype: tuanth.nc.uit@gmail.com